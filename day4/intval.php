<?php
/**
intval — Get the integer value of a variable
 */
echo intval(42)."<br>" ;
echo intval('42')."<br>";
echo intval(bangla)."<br>";
echo intval("bangla")."<br>";
echo intval(42,0)."<br>";
echo intval("42",0)."<br>";
echo intval(42,8)."<br>";
echo intval(array())."<br>";
echo intval(array('gg',25))."<br>";
echo intval(42000000000)."<br>";