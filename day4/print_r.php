<?php
/**
print_r — Prints human-readable information about a variable
 */
$value=array('a'=>'apple','b'=>'banana','c'=>array('a','b','c'));
print_r($value);